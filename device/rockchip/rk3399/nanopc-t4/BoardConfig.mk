include device/rockchip/rk3399/BoardConfig.mk

TARGET_BOARD_PLATFORM_PRODUCT := tablet

DEVICE_PACKAGE_OVERLAYS := device/rockchip/rk3399/nanopc-t4/overlay
PRODUCT_USE_PREBUILT_GTVS := yes

PRODUCT_KERNEL_DTS := nanopi4-images
PRODUCT_KERNEL_CONFIG := nanopi4_android_defconfig android-10.config rk3399.config
PRODUCT_UBOOT_CONFIG := nanopi4

BOARD_SELINUX_ENFORCING := false

BOARD_SENSOR_ST := true
BOARD_SENSOR_MPU_PAD := false
BOARD_HAS_RK_4G_MODEM := false
BUILD_WITH_GOOGLE_GMS_EXPRESS := false

BOARD_PREBUILT_DTBOIMAGE := device/rockchip/rk3399/nanopc-t4/dtbo.img

# AB image definition
BOARD_USES_AB_IMAGE := false

ifeq ($(strip $(BOARD_USES_AB_IMAGE)), true)
    include device/rockchip/common/BoardConfig_AB.mk
    TARGET_RECOVERY_FSTAB := device/rockchip/rk3399/nanopc-t4/recovery.fstab_AB
endif

ifeq ($(BOARD_HAVE_DONGLE),true)
DEVICE_MANIFEST_FILE += device/rockchip/rk3399/nanopc-t4/radio.xml
endif

ifeq ($(BOARD_HAS_GPS),true)
DEVICE_MANIFEST_FILE += device/rockchip/rk3399/nanopc-t4/mgnss.xml
endif

#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# First lunching is Q, api_level is 29
PRODUCT_SHIPPING_API_LEVEL := 29

$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)
include device/rockchip/rk3399/nanopc-t4/BoardConfig.mk

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.drm.sh:$(TARGET_COPY_OUT_VENDOR)/bin/init.drm.sh \
    $(LOCAL_PATH)/pwm_fan.sh:$(TARGET_COPY_OUT_VENDOR)/bin/pwm_fan.sh

# Camera
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/camera3_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/camera/camera3_profiles.xml \
    $(LOCAL_PATH)/media_profiles_V1_0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

# Quectel
$(call inherit-product-if-exists, vendor/quectel/ec20/device-partial.mk)

HAVE_PREBUILT_DTBO := $(shell test -f $(BOARD_PREBUILT_DTBOIMAGE) && echo true)
ifneq ($(HAVE_PREBUILT_DTBO),true)
PRODUCT_BOOT_DEVICE := fe330000.sdhci
PRODUCT_DTBO_TEMPLATE := $(LOCAL_PATH)/dt-overlay.in
endif

PRODUCT_FSTAB_TEMPLATE := $(LOCAL_PATH)/fstab.in

include device/rockchip/common/build/rockchip/DynamicPartitions.mk
include device/rockchip/common/BoardConfig.mk

# Inherit from those products. Most specific first.
$(call inherit-product, device/rockchip/rk3399/device.mk)
$(call inherit-product, device/rockchip/common/device.mk)

#enable this for support f2fs with data partion
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := ext4

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_NAME := nanopc_t4
PRODUCT_DEVICE := nanopc-t4
PRODUCT_BRAND := Android
PRODUCT_MODEL := NanoPC-T4 (RK3399)
PRODUCT_MANUFACTURER := FriendlyELEC (www.friendlyarm.com)

# Screen size is "normal", density is "hdpi"
PRODUCT_AAPT_CONFIG := normal large xlarge mdpi hdpi xhdpi
PRODUCT_AAPT_PREF_CONFIG := hdpi

PRODUCT_PACKAGES += \
    SoundRecorder

PRODUCT_PROPERTY_OVERRIDES += \
    vendor.gralloc.disable_afbc=1 \
    vendor.hwc.device.primary=eDP,HDMI-A \
    vendor.hwc.device.extend=HDMI-A \
    persist.demo.hdmirotates=true \
    ro.vendor.user_rotation=true \
    ro.product.version=1.0.0 \
    ro.rksdk.version=android-10.0-mid-rkr6

# Get the long list of APNs
PRODUCT_COPY_FILES += vendor/rockchip/common/phone/etc/apns-full-conf.xml:system/etc/apns-conf.xml
PRODUCT_COPY_FILES += vendor/rockchip/common/phone/etc/spn-conf.xml:system/etc/spn-conf.xml

ifeq ($(BUILD_WITH_EC20),true)
PRODUCT_PACKAGES += \
    android.hardware.broadcastradio@1.0-impl \
    android.hardware.radio.config@1.0-service \
    CarrierDefaultApp \
    CarrierConfig \
    messaging
endif

PRODUCT_PACKAGES += \
    fsck.exfat \
    mkfs.exfat

#PRODUCT_HAVE_OPTEE := true

# These are the hardware-specific features
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.camera.full.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.full.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.stepdetector.xml \
    frameworks/native/data/etc/android.hardware.sensor.hifi_sensors.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.hifi_sensors.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
    frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/native/data/etc/android.hardware.telephony.cdma.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.telephony.cdma.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.ethernet.xml

# Audio Policy tables
PRODUCT_COPY_FILES += \
    frameworks/av/services/audiopolicy/config/a2dp_in_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_in_audio_policy_configuration.xml

# Privileged permissions whitelist
PRODUCT_COPY_FILES += \
    device/rockchip/rk3399/permissions/privapp-permissions-rockchip.xml:system/etc/permissions/privapp-permissions-rockchip.xml

PRODUCT_PROPERTY_OVERRIDES += \
    ro.control_privapp_permissions=enforce

# vendor apps
$(call inherit-product-if-exists, vendor/friendlyelec/apps/device-partial.mk)

BUILD_WITHOUT_VENDOR_APPS := RkApkinstaller RkExplorer RKUpdateService userExperienceService

# google gms
ifeq ($(INSTALL_GAPPS_FOR_TESTING), yes)
BUILD_WITHOUT_VENDOR_APPS += Lightning
$(call inherit-product-if-exists, vendor/google/gapps/device-partial.mk)
endif

